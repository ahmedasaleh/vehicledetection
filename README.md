# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* The code in this repository is taken from https://bitbucket.org/rodrigob/doppia and modified by removing the stixel code. The goal is to make the code ready for mono-camera, compile under NVidia Nsight 6.5 IDE, then test over NVidia Jetson TK1 board.
* Version: 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up: NVIDIA GPU, CUDA 5.5 at least (this code tested using CUDA 6.5), UBUNTU 14.04 TLS, OpenCV 2.4, GCC 4.8
* Configuration: you can either clone the projects as is into your NVidia Nsight workspace or create CUDA projects in your workspace then import them. 
* Dependencies: CUDA, boost, eigen, liblinear, opencv, libjpeg, libpng
* How to run: build projects either in release mode or debug mode. Release version is faster than debug version 100-200 times! 

### Contribution guidelines ###

* TBD

### Who do I talk to? ###

* Repo owner: ahmedamsaleh@gmail.com